import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http: HttpClient) { }

  get(path: string): Observable<any> {
    const url = path
    return this.http.get<any>(url)
  }
  patch(path: string, params: any): Observable<any> {
    const url = path
    return this.http.patch<any>(url, params)
  }
  delete(path: string): Observable<any> {
    const url = path
    return this.http.delete<any>(url)
  }

}
